import java.util.Scanner;



public class AlgoritmoAmicizia {

    public static Scanner s = new Scanner(System.in);

    public static boolean isSi(String s){
        return ("sSyY".contains(String.valueOf(s.charAt(0))));
    }

    public static boolean rosso(){
        boolean amici = false;

        System.out.println("Sei in casa? ");
        String risposta = s.nextLine();
        
        while(!isSi(risposta)){
            System.out.println(" aspetta di essere richiamato ");
            System.out.println("Sei in casa? "); 
            risposta = s.nextLine();
        }

        System.out.println("Ti va di mangiare qualcosa insieme? ");
        risposta = s.nextLine();

        if(isSi(risposta)){
            return true;
        }
        
        return false;
    }


    public static boolean blu(){
        
        System.out.println("E di bere qualcosa di caldo?");
        String risposta = s.nextLine().trim();

        if (!isSi(risposta)) {
            return false;
        }
        else {
            System.out.println("Scegli: Te, Caffé o Cioccolata? ");
            String risposta_bevanda = s.nextLine().toLowerCase().trim();
            switch (risposta_bevanda){
                case "te":
                    System.out.println("Fatevi sto te");
                    return true;
                case "caffé":
                    System.out.println("Fatevi sto Caffé");
                    return true;
                case "cioccolata":
                    System.out.println("Fatevi sta Cioccolata");
                    return true;
            }
        }
        return false;
    }

    public static boolean verde(){
        int n = 0;
		String result="";
		System.out.println("Allora svagliamoci un' po'");
		
		while(n < 5){
            System.out.print("Cosa altro ti va di fare ? ");
			result = (s.nextLine()).toLowerCase().trim();
			if(result.equals("arrampicata")){
				break;
			}
			n++;
		}
		System.out.print("Gli va di fare" + result);
		return true;
    }

    

    
    public static void main(String[] args) {
        if (rosso()){
            System.out.println("Siete amici ");
        } else if(blu()) {}
        else if(verde()){}

        s.close();
    }
}
