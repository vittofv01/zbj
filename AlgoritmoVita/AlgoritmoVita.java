import java.util.Scanner;

public class AlgoritmoVita {

    public static Scanner s = new Scanner(System.in);
    public static String nome = "";

    public static boolean isSi(String s) {
        return ("sSyY".contains(String.valueOf(s.charAt(0))));
    }

    public static void scuola() {
        System.out.println("Dopo 6 anni hai iniziato le elementari.");
        System.out.println("Dopo 11 anni hai iniziato le medie.");
        System.out.println("Dopo 14 anni hai iniziato le superiori, com'è andata?");
        int n_bocciature = 0;
        int anno_superiori = 1;
        String risposta;
        while (anno_superiori <= 5) {
            if (n_bocciature < 3) {
                System.out.println("Hai seguito il " + anno_superiori + " anno delle superiori, sei stato bocciato?");
                risposta = s.nextLine();
                if (isSi(risposta)) {
                    n_bocciature++;
                    System.out.println("Sei stato bocciato " + n_bocciature);
                } else {
                    anno_superiori++;
                }
            } else {
                break;
            }
        }
        System.out.println("Complimenti sei uscito da scuola completando " + (anno_superiori - 1) + " anni con "
                + n_bocciature + " bocciature");

        return;
    }

    public static boolean lavoro(){
        int anno = 0;
        while (anno <= 20){
            anno++;
            System.out.println("anno di lavoro numero " + anno);
        }
        
        System.out.println("Infortunio debilitante? ");
        String risposta = s.nextLine();

        if (isSi(risposta)){
            return true;
        }
        else{
           return false;
        }

    }


    public static void rosso() {
        System.out.println("Fai gli ultimi anni di lavoro ");
        String risposta = s.nextLine();

        for (int i = 0; i < 3; i++) {
            System.out.println("Vieni promosso? ");
            risposta = s.nextLine();
            if (!isSi(risposta)) {
                return;
            }
        }

        System.out.println("Ora sei capo dell'azienda");
        risposta = s.nextLine();
        return;
    }

    public static void main(String[] args) {
        System.out.print("Inserisci il tuo nome: ");
        nome = s.nextLine();
        System.out.println("Complimenti " + nome + " sei nato.");
        scuola();
        if (!lavoro()){
            rosso();
        }
        

        System.out.println("Vai in pensione");
        s.close();
    }
}